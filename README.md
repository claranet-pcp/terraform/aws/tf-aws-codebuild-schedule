# tf-aws-codebuild-schedule

A module to enable you to easily setup a schedule for triggering a CodeBuild project. It creates a CloudWatch event rule triggering with the specified schedule to trigger a Lambda that then triggers the CodeBuild project.

This is required as there is no way to directly setup a schedule on CodeBuild projects using Terraform so you must setup a Lambda to trigger the CodeBuild.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| cloudwatch\_event\_rule\_name\_prefix | The name prefix for the Cloudwatch Event Rule. | string | n/a | yes |
| codebuild\_project\_arn | The arn of the Codebuild project to trigger. | string | n/a | yes |
| codebuild\_project\_name | The name of the Codebuild project to trigger. | string | n/a | yes |
| codebuild\_trigger\_lambda\_name | The name to use for the Lambda that triggers the Codebuild project. | string | n/a | yes |
| schedule\_expression | The schedule expression to use for triggering this event. See https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html | string | n/a | yes |
| schedule\_enabled | Whether the schedule is enabled or not. | string | `"true"` | no |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
