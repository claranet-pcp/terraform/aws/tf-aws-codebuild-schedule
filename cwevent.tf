resource "aws_cloudwatch_event_rule" "event" {
  name_prefix         = var.cloudwatch_event_rule_name_prefix
  description         = "Scheduled execution of the ${var.codebuild_project_name} AWS Codebuild project."
  schedule_expression = var.schedule_expression
  is_enabled          = var.schedule_enabled
}

resource "aws_cloudwatch_event_target" "target" {
  arn  = module.codebuild_trigger_lambda.function_arn
  rule = aws_cloudwatch_event_rule.event.name
}
