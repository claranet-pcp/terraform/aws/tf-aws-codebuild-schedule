data "aws_iam_policy_document" "lambda" {
  statement {
    sid = "AllowCodeBuildProjectTrigger"
    actions = [
      "codebuild:StartBuild"
    ]
    resources = [
      var.codebuild_project_arn
    ]
  }
}
