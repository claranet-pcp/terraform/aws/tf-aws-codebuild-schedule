import os
import boto3

def handler(context, event):
    codebuild = boto3.client("codebuild")
    codebuild.start_build(projectName=os.environ['CODEBUILD_PROJECT_NAME'])
