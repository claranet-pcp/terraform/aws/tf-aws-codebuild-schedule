module "codebuild_trigger_lambda" {
  source = "github.com/claranet/terraform-aws-lambda?ref=v1.1.0"

  function_name = var.codebuild_trigger_lambda_name
  description   = "Lambda to trigger the ${var.codebuild_project_name} project on a schedule."
  handler       = "lambda.handler"
  runtime       = "python3.6"
  timeout       = 15

  source_path = "${path.module}/lambda.py"

  policy = {
    json = data.aws_iam_policy_document.lambda.json
  }

  // Add environment variables.
  environment = {
    variables = {
      CODEBUILD_PROJECT_NAME = var.codebuild_project_name
    }
  }
}

resource "aws_lambda_permission" "cwevent_lambda_permission" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = module.codebuild_trigger_lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.event.arn
}
