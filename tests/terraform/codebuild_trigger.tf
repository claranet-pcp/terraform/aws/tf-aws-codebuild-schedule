module "codebuild_trigger" {
  source                            = "../../"
  codebuild_project_name            = "${aws_codebuild_project.test.name}"
  codebuild_project_arn             = "${aws_codebuild_project.test.arn}"
  schedule_expression               = "rate(5 minutes)"
  cloudwatch_event_rule_name_prefix = "codebuild-trigger-test"
  codebuild_trigger_lambda_name     = "codebuild-trigger-test"
}