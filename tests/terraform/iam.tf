data "aws_iam_policy_document" "codebuild_assume_role_policy" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "codebuild.amazonaws.com",
      ]
    }
  }
}

resource "aws_iam_role" "test_codebuild_role" {
  name_prefix        = var.codebuild_iam_role_name_prefix != null ? var.codebuild_iam_role_name_prefix : "test-codebuild-role"
  assume_role_policy = data.aws_iam_policy_document.codebuild_assume_role_policy.json
}
