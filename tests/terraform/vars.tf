variable "codebuild_project_name" {
  default = "aws-codebuild-trigger-test-project"
}

variable "cloudwatch_logs_group_name" {
  default = "terraform-tests"
}

variable "cloudwatch_logs_stream_name" {
  default = "codebuild-trigger"
}

variable "codebuild_iam_role_name_prefix" {
  description = "The IAM role name prefix to use for the role created for the Codebuild project."
  type        = string
  default     = null
}
