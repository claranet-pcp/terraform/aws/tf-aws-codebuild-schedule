variable "codebuild_project_name" {
  description = "The name of the Codebuild project to trigger."
  type        = string
}

variable "codebuild_project_arn" {
  description = "The arn of the Codebuild project to trigger."
  type        = string
}

variable "codebuild_trigger_lambda_name" {
  description = "The name to use for the Lambda that triggers the Codebuild project."
  type        = string
}

variable "schedule_expression" {
  description = "The schedule expression to use for triggering this event. See https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html"
  type        = string
}

variable "cloudwatch_event_rule_name_prefix" {
  description = "The name prefix for the Cloudwatch Event Rule."
  type        = string
}

variable "schedule_enabled" {
  description = "Whether the schedule is enabled or not."
  default     = true
}

